#!/usr/bin/env python
from labrider_lib.labrider import Robot
from labrider_lib.MapController import Map
import rospy
import math
import time

map = Map()
robot = Robot("labrider")

while robot.is_ok():
    for x in range(0, map.width):
        for y in range(0, map.height):
            if map.blocked(x, y):
                map.value[x, y] = -1
            else:
                map.value[x, y] = 0

    fill_value = 1

    map.value[map.finishX, map.finishY] = fill_value

    while map.value[map.startX, map.startY] == 0:
        for x in range(0, map.width):
            for y in range(0, map.height):
                if map.value[x, y] == fill_value:
                    if(map.value[x+1, y] == 0):
                        map.value[x+1, y] = fill_value + 1
                    if(map.value[x-1, y] == 0):
                        map.value[x-1, y] = fill_value + 1
                    if(map.value[x, y-1] == 0):
                        map.value[x, y-1] = fill_value + 1
                    if(map.value[x, y+1] == 0):
                        map.value[x, y+1] = fill_value + 1
        fill_value += 1

    while not robot.is_arrived():        
        if robot.front == robot.center - 1:
            robot.move_forward()
        elif robot.right == robot.center - 1:
            robot.turn_right()
        elif robot.left == robot.center - 1:
            robot.turn_left()
        elif robot.back == robot.center -1:
            robot.move_backward()
