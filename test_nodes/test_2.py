#!/usr/bin/env python
from labrider_lib.labrider import Robot
from labrider_lib.MapController import Map
import rospy
import math
import time


robot = Robot("labrider")
map = Map()

map.finishX = 10
map.finishY = 4

#Your Code Starts Here
for x in range(0, map.width):
    for y in range(0, map.height):
        if map.blocked(x, y):
            map.value[x, y] = -1
        else:
            map.value[x, y] = 0


fill_value = 1
map.value[map.startX, map.startY] = fill_value

while map.value[map.finishX, map.finishY] == 0:
    for x in range(0, map.width):
        for y in range(0, map.height):
            if map.value[x, y] == fill_value:
                if(map.value[x+1, y] == 0):
                    map.value[x+1, y] = fill_value + 1
                if(map.value[x-1, y] == 0):
                    map.value[x-1, y] = fill_value + 1
                if(map.value[x, y+1] == 0):
                    map.value[x, y+1] = fill_value + 1
                if(map.value[x, y-1] == 0):
                    map.value[x, y-1] = fill_value + 1
    fill_value = fill_value + 1

finish_value = map.value[map.finishX, map.finishY]
for x in range(0, map.width):
    for y in range(0, map.height):
        if map.value[x, y] == finish_value:
            if not (x == map.finishX and y == map.finishY):
                map.value[x,y] = 0

currentX = map.finishX
currentY = map.finishY
reverse_value = map.value[currentX, currentY]
while reverse_value != 1:
    for x in range(0, map.width):
        for y in range(0, map.height):
            if map.value[x, y] == reverse_value - 1:
                dx = currentX - x
                dy = currentY - y
                if(abs(dx) + abs(dy)) > 1:
                    map.value[x, y] = 0

    for x in range(0, map.width):
        for y in range(0, map.height):
            if map.value[x, y] == reverse_value - 1:
                currentX = x
                currentY = y
    reverse_value = map.value[currentX, currentY]


proceed = True
while proceed:
    dirx = int(math.cos(robot.dir))
    diry = int(math.sin(robot.dir))

    center = map.value[robot.posX, robot.posY]
    front = map.value[robot.posX + dirx, robot.posY + diry]
    right = map.value[robot.posX + diry, robot.posY - dirx]
    left = map.value[robot.posX - diry, robot.posY + dirx]

    proceed = False
    if front == center + 1:
        robot.move_forward()
        proceed = True
    elif right == center + 1:
        robot.turn_right()
        proceed = True
    elif left == center + 1:
        robot.turn_left()
        proceed = True