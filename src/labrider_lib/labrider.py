#!/usr/bin/env python
from robot_control import RobotControl
import rospy
from std_srvs.srv import Trigger, TriggerRequest
import math
from std_msgs.msg import String
import time
from angles import normalize_angle
import os
import requests
from labrider_lib.srv import HeatStatus

class Robot:
    def __init__(self, robot_name):

        rospy.init_node("labrider_controller")
        self.robot_pose_pub = rospy.Publisher("/robot_pose/", String, queue_size=10)

        #Labrider
        robot_data = {
        "robot_name": robot_name,
        "base_link": "chassis",
        "left_wheels": {'wheel_FL_link': (0.19283), 'wheel_BL_link': (0.19283)},
        "right_wheels": {'wheel_FR_link': (0.19283), 'wheel_BR_link': (0.19283)},
        "wheel_pos": [0.34, 0.23]
        }

        pos_data = {
        "x": (-4.5),
        "y": (-3.5),
        "z": (0.353),
        "yaw": (math.pi/2)
        }

        #Husky
        """
        robot_data = {
        "robot_name": robot_name,
        "base_link": "base_link",
        "left_wheels": {'front_left_wheel_link': (0.1651), 'rear_left_wheel_link': (0.1651)},
        "right_wheels": {'front_right_wheel_link': (0.1651), 'rear_right_wheel_link': (0.1651)},
        "wheel_pos": [0.256, 0.2854]
        }

        pos_data = {
        "x": (-4.5),
        "y": (-3.5),
        "z": (0.133),
        "yaw": (math.pi/2)
        }
        """

        event_data = {
        "unit_length": (1),
        "event_period": (0.75)
        }

        self.robot_name = robot_name
        self.pos_x = pos_data['x']
        self.pos_y = pos_data['y']
        self.dir = pos_data['yaw']

        self.posX = int(self.pos_x + 5.5)
        self.posY = int(self.pos_y + 4.5)

        self.point_a = [1, 1]
        self.point_b = [2, 4]
        self.point_c = [7, 8]
        self.point_d = [10, 4]

        self.finish_x = self.point_b[0]
        self.finish_y = self.point_b[1]

        self.status = "Stop"

        self.init_services()
        self.init_game_controller()

        self.checkpoint_6_count = 0
        self.checkpoint_6_flag = False

        if not self.status == "Stop" and not self.status == "no_robot":
            self.robot = RobotControl(robot_data, pos_data, event_data)

    def init_services(self):
        try:
            rospy.wait_for_service("/get_surrounding_heats", 1.0)
            self.get_surrounding_heats = rospy.ServiceProxy('get_surrounding_heats', HeatStatus)
            time.sleep(0.1)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/change_finish", 1.0)
            self.change_finish_point = rospy.ServiceProxy('/change_finish', Trigger)
            time.sleep(0.1)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))


    def init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 1.0)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            time.sleep(0.1)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()
            self.status = resp.message
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "free_roam"
            rospy.logerr("Service call failed: %s" % (e,))


    def check_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 5.0)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()

            self.status = resp.message
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "Stop"
            rospy.logerr("Service call failed: %s" % (e,))


    def move_forward(self):
        if not self.status == "Stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.move_distance(1)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            self.posX = int(self.pos_x + 5.5)
            self.posY = int(self.pos_y + 4.5)

            if self.posX == 2 and self.posY == 4:
                # fifth checkpoint
                #print("fifth")
                self.achieve(86)

            if not self.status == "free_roam":
                self.check_game_controller()

            if self.status == "run":
                resp = self.robot_handle()
                self.status = resp.message


    def move_backward(self):
        if not self.status == "Stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.move_distance(-1)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            self.posX = int(self.pos_x + 5.5)
            self.posY = int(self.pos_y + 4.5)

            if not self.status == "free_roam":
                self.check_game_controller()

            if self.status == "run":
                resp = self.robot_handle()
                self.status = resp.message

            if self.posX == 2 and self.posY == 4:
                # fifth checkpoint
                #print("fifth")
                self.achieve(86)
    

    def turn_left(self):
        if not self.status == "Stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.rotate_angle(math.pi/2)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            self.posX = int(self.pos_x + 5.5)
            self.posY = int(self.pos_y + 4.5)

            if not self.status == "free_roam":
                self.check_game_controller()

            if self.status == "run":
                resp = self.robot_handle()
                self.status = resp.message


    def turn_right(self):
        if not self.status == "Stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.rotate_angle(-math.pi/2)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            self.posX = int(self.pos_x + 5.5)
            self.posY = int(self.pos_y + 4.5)

            if not self.status == "free_roam":
                self.check_game_controller()

            if self.status == "run":
                resp = self.robot_handle()
                self.status = resp.message


    @property
    def center(self):
        resp = self.get_surrounding_heats('', self.pos_x, self.pos_y)
        
        return resp.center_heat

    @property
    def front(self):
        resp = self.get_surrounding_heats('', self.pos_x, self.pos_y)

        if abs(normalize_angle(self.dir)) < 0.01:
            return resp.east_heat
        elif abs(normalize_angle(self.dir - math.pi/2)) < 0.01:
            return resp.north_heat
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01:
            return resp.west_heat
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01:
            return resp.south_heat

    @property
    def left(self):
        resp = self.get_surrounding_heats('', self.pos_x, self.pos_y)

        if abs(normalize_angle(self.dir)) < 0.01:
            return resp.north_heat
        elif abs(normalize_angle(self.dir - math.pi/2)) < 0.01:
            return resp.west_heat
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01:
            return resp.south_heat
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01:
            return resp.east_heat

    @property
    def back(self):
        resp = self.get_surrounding_heats('', self.pos_x, self.pos_y)

        if abs(normalize_angle(self.dir)) < 0.01:
            return resp.west_heat
        elif abs(normalize_angle(self.dir - math.pi/2)) < 0.01:
            return resp.south_heat
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01:
            return resp.east_heat
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01:
            return resp.north_heat

    @property
    def right(self):
        resp = self.get_surrounding_heats('', self.pos_x, self.pos_y)

        if abs(normalize_angle(self.dir)) < 0.01:
            return resp.south_heat
        elif abs(normalize_angle(self.dir - math.pi/2)) < 0.01:
            return resp.east_heat
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01:
            return resp.north_heat
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01:
            return resp.west_heat

    def is_ok(self):
        if not rospy.is_shutdown() and not self.status == "no_robot" and not self.status == "Stop":
            return True

        return False

    def is_arrived(self):
        if self.posX == self.finish_x and self.posY == self.finish_y:
            if self.finish_x == self.point_b[0] and self.finish_y == self.point_b[1]:
                self.finish_x = self.point_c[0]
                self.finish_y = self.point_c[1]
                self.change_finish_point()
                self.checkpoint_6_count += 1

            elif self.finish_x == self.point_c[0] and self.finish_y == self.point_c[1]:
                self.finish_x = self.point_d[0]
                self.finish_y = self.point_d[1]
                self.change_finish_point()
                self.checkpoint_6_count += 1

            elif self.finish_x == self.point_d[0] and self.finish_y == self.point_d[1]:
                # sixth checkpoint
                if self.checkpoint_6_count == 2 and not self.checkpoint_6_flag == True:
                    self.checkpoint_6_flag = True
                    #print("sixth")
                    self.achieve(87)

                self.change_finish_point()

            return True
        else:
            return False

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })
